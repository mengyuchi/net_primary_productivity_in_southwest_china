import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
from pylab import *
import matplotlib.font_manager as font_manager
import matplotlib.patches as mpatches


# import matplotlib.pyplot as plt
plt.rc('font',family='Times New Roman')
# for font in font_manager.fontManager.ttflist:
#     print(font)


# Img 1

row = [0,1]
col = [0,1,2]
index = [1,2,3,4,5,6]

font_size = 18

# %config InlineBackend.print_figure_kwargs = {'bbox_inches':None}

fig, axs = plt.subplots(2, 3, sharex=True, sharey=True, constrained_layout=True, figsize=(19, 13))
# fig.subplots_adjust(hspace=0.1)
# plt.subplots_adjust(left=None, bottom=None, right=None, top=None, wspace=None, hspace=0.5)

for i in index:
    plt.subplot(2,3,i)
    # ax = axs[i]
    inputindex =  str(i) 
    corr_data =  pd.read_excel("../net_primary_productivity_in_southwest_china/data/data.xlsx",sheet_name=inputindex)
    corr_data_new =  corr_data.rename(columns={'X1':'$X_1$','X2':'$X_2$','X3':'$X_3$','X4':'$X_4$','X5':'$X_5$','X6':'$X_6$'}, 
                                    index={0: '$X_1$',1: '$X_2$',2: '$X_3$',3: '$X_4$',4: '$X_5$',5: '$X_6$'})
    mask = np.zeros_like(corr_data_new)
    mask[np.triu_indices_from(mask)] = True

    # print(corr_data_new)
    ax = sns.heatmap(
        corr_data_new, # 相关性系数值数据
        annot=True, # 是否显示数据
        center=0.5, # 设置色彩居中值
        # mask = mask, # 数据显示在mask为False的单元格中
        fmt='.2f', # 设置数值显示的小数位数
        # linewidths=.2, # 设置每个单元格之间的间距线
        linecolor="red", # 设置间距线的颜色
        vmin=0, # 设置数值最小值
        vmax=1, # 设置数值最大值
        xticklabels= True, # 是否显示X轴字段
        yticklabels= True, # 是否显示Y轴字段
        square=True, # 是否每个方格都是正方形
        cbar=False, # 是否绘制颜色条
        # cmap="coolwarm_r", # 设置热力图颜色配色
        cmap="coolwarm_r", # 设置热力图颜色配色
        annot_kws={"fontsize":font_size} # 设置热力图字号
        )
    a = ax.pcolormesh(corr_data_new, vmin=0.00, vmax=1.00, cmap="coolwarm_r")

    ax.tick_params(labelsize=font_size)
    # plt.subplots_adjust(left=None, bottom=None, right=None, top=None, wspace=None, hspace=0)

cb_ax = fig.add_axes([1.03, 0.1, 0.02, 0.8]) #设置colarbar位置
# plt.rcParams['font.size'] = 13
cbar = fig.colorbar(a, cax=cb_ax,format='%.2f')     #共享colorbar
cbar.ax.tick_params(labelsize = font_size)

# plt.tight_layout()
plt.show()
fig.savefig('test.png', bbox_inches='tight', dpi=600)

# IMG 2

index = [1,2,3,4,5,6]

font_size = 18

# %config InlineBackend.print_figure_kwargs = {'bbox_inches':None}

fig, axs = plt.subplots(2, 3, sharex=False, sharey=False, constrained_layout=True, figsize=(19, 13))
# fig.subplots_adjust(hspace=0.1)
# plt.subplots_adjust(left=None, bottom=None, right=None, top=None, wspace=None, hspace=0.5)

for i in index:
    plt.subplot(2,3,i)
    # ax = axs[i]
    inputindex =  str(i) 
    corr_data =  pd.read_excel("../net_primary_productivity_in_southwest_china/data/data2.xlsx",sheet_name=inputindex)
    corr_data_new =  corr_data.rename(columns={'X1':'$X_1$','X2':'$X_2$','X3':'$X_3$','X4':'$X_4$','X5':'$X_5$','X6':'$X_6$'}, 
                                    index={0: '$X_1$',1: '$X_2$',2: '$X_3$',3: '$X_4$',4: '$X_5$',5: '$X_6$'})
    mask = np.zeros_like(corr_data_new)
    mask[np.triu_indices_from(mask)] = True

    # print(corr_data_new)
    ax = sns.heatmap(
        corr_data_new, # 相关性系数值数据
        annot=False, # 是否显示数据
        center=0.5, # 设置色彩居中值
        # mask = mask, # 数据显示在mask为False的单元格中
        fmt='.2f', # 设置数值显示的小数位数
        linewidths=0.3, # 设置每个单元格之间的间距线
        linecolor="white", # 设置间距线的颜色
        vmin=0, # 设置数值最小值
        vmax=1, # 设置数值最大值
        xticklabels= True, # 是否显示X轴字段
        yticklabels= True, # 是否显示Y轴字段
        square=True, # 是否每个方格都是正方形
        cbar=False, # 是否绘制颜色条
        # cmap="coolwarm_r", # 设置热力图颜色配色
        cmap="gist_ncar_r", # 设置热力图颜色配色
        annot_kws={"fontsize":font_size} # 设置热力图字号
        )
    # a = ax.pcolormesh(corr_data_new, vmin=0, vmax=1, cmap="coolwarm_r")

    ax.tick_params(labelsize=font_size)
    # plt.subplots_adjust(left=None, bottom=None, right=None, top=None, wspace=None, hspace=0)

# cb_ax = fig.add_axes([1.03, 0.1, 0.02, 0.8]) #设置colarbar位置
# # plt.rcParams['font.size'] = 13
# cbar = fig.colorbar(a, cax=cb_ax)     #共享colorbar
# cbar.ax.tick_params(labelsize = font_size)
# plt.rcParams["font.sans-serif"]=["SimHei"] #设置字体
matplotlib.rc("font",family='SimHei')
yes_patch = mpatches.Patch(color='chartreuse', label='Y 显著性差异')
no_patch = mpatches.Patch(color='orange', label='N 无显著性差异')
fig.legend(handles=[yes_patch, no_patch],loc=1,prop = {'size':14},bbox_to_anchor=(1, 0.85))
# plt.legend(loc='best', bbox_to_anchor=(0.5, 0., 0.5, 0.5))


# plt.tight_layout()
plt.show()
fig.savefig('test1.png', bbox_inches='tight', dpi=600)